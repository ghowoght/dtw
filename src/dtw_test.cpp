/**
 * @file dtw_test.cpp
 * @brief 
 * @author Linfu Wei (ghowoght@qq.com)
 * @version 1.0
 * @date 2021-10-24
 * 
 * @copyright Copyright (c) 2021  WHU-I2NAV
 * 
 */
#include <iostream>
#include "dtw.hpp"
#include <vector>
#include <fstream>
#include <istream>

int main(int argc, char** argv){
    std::vector<double> xList;
    std::vector<double> yList;

    std::ifstream data_fs("./data/data.txt", std::ios::in);

    while(!data_fs.eof()){
        double x, y;
        data_fs >> x >> y;
        xList.push_back(x);
        yList.push_back(y);
    }    
    data_fs.close();

    std::ofstream result_fs("./data/result.txt");

    DTW dtw(xList, yList);

    if(dtw.search()){
        auto path = dtw.getPath();
        for(auto p : path){
            std::cout << p[0] << ", " << p[1] << std::endl;
            result_fs << xList[p[0]] << " " << yList[p[1]] << " ";
            result_fs << p[0] << " " << p[1] << std::endl;
        }
        result_fs.close();
    }
    
}