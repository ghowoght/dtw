import matplotlib.pyplot as plt
import numpy as np
import math

data = np.loadtxt("./data/result.txt")

xList = data[:, 0]
yList = data[:, 1] + 1
xIndex = data[:, 2]
yIndex = data[:, 3]

for k in range(0, len(xList), 5):
    line = [[xIndex[k], xList[k]], [yIndex[k], yList[k]]]
    line = np.asarray(line)
    plt.plot(line[:, 0], line[:, 1], linewidth=1)

plt.plot(xIndex, xList, 'r', linewidth=3)
plt.plot(yIndex, yList, 'g', linewidth=3)
plt.xlabel("sample num")

plt.show()